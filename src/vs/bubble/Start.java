package vs.bubble;

public class Start {

	public static void main(String[] args) {
		int[] szamok = { 5, 4, 3, 2, 1 };
		boolean cserelt = false;
		for (int j = 0; j < szamok.length - 1; j++) {

			for (int i = 0; i < szamok.length - 1; i++) {
				if (szamok[i] > szamok[i + 1]) {
					// akkor csere
//					csere();
//				int tmp = szamok[i];
//				szamok[i] = szamok[i + 1];
//				szamok[i + 1] = tmp;
					csere(szamok,i);
					cserelt = true;
				}
				printTomb(szamok);

			}
			if (!cserelt) {
				break;
			}
		}
	}

	static void univerzalisCsere(int[] sz, int idx1, int idx2)
	{
		int tmp = sz[idx1];
		sz[idx1] = sz[idx2];
		sz[idx2] = tmp;
	}
	
	static void csere(int[] sz, int i)
	{
		int tmp = sz[i];
		sz[i] = sz[i+1];
		sz[i+1] = tmp;
	}



	static void printTomb(int[] tomb) {
		for (int i = 0; i < tomb.length; i++) {
			System.out.print(tomb[i] + ",");
		}
		System.out.println();
	}
}
